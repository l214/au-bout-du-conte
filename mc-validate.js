// From : https://css-tricks.com/form-validation-part-4-validating-mailchimp-subscribe-form/

//Init
var forms = document.querySelectorAll('.validate');
for (var i = 0; i < forms.length; i++) {
    // Add the novalidate attribute when the JS loads
    forms[i].setAttribute('novalidate', true);
}

// Validate the field
var hasError = function (field) {

    // Don't validate submits, buttons, file and reset inputs, and disabled fields
    if (field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') {
        return;
    }

    // Get validity
    var validity = field.validity;

    // If valid, return null
    if (validity.valid) {
        return;
    }

    // If field is required and empty
    if (validity.valueMissing) {
        return 'Ce champ est requis.';
    }

    // If not the right type
    if (validity.typeMismatch) {
        // Email
        if (field.type === 'email') {
            return 'Veuillez entrer une adresse email valide.';
        }
    }

    // If all else fails, return a generic catchall error
    return 'Veuillez entrer une valeur valide pour ce champs.';
};


// Show an error message
var showError = function (field, error) {

    // Add error class to field
    field.classList.add('error');
  
    // If the field is a radio button and part of a group, error all and get the last item in the group
    if (field.type === 'radio' && field.name) {
        var group = field.form.querySelectorAll('[name="' + field.name + '"]');
        if (group.length > 0) {
            for (var i = 0; i < group.length; i++) {
                group[i].classList.add('error');
            }
            field = group[group.length - 1];
        }
    }

    // Get field id or name
    var id = field.id || field.name;
    if (!id) {
        return;
    }

    // Check if error message field already exists
    // If not, create one
    var message = field.form.querySelector('.mc-error#error-for-' + id);
    if (!message) {
        message = document.createElement('div');
        message.className = 'mc-error';
        message.id = 'error-for-' + id;
        
        // If the field is a radio button or checkbox, insert error after the label
        var label;
        if (field.type === 'radio' || field.type === 'checkbox') {
            label = field.form.querySelector('label[for="' + id + '"]') || field.parentNode;
            if (label) {
                label.parentNode.insertBefore(message, label.nextSibling);
            }
        }

        // Otherwise, insert it after the field
        if (!label) {
            field.parentNode.insertBefore(message, field.nextSibling);
        }
    }
    
    // Add ARIA role to the field
    field.setAttribute('aria-describedby', 'error-for-' + id);

    // Update error message
    message.innerHTML = error;

    // Show error message
    message.style.display = 'block';
    message.style.visibility = 'visible';

};


// Remove all error messages
var removeAllErrors = function (form) {
    var fields = form.elements;
    for (var i = 0; i < fields.length; i++) {
       removeError(fields[i]);
    }
};

// Remove the error message
var removeError = function (field) {

    // Remove error class to field
    field.classList.remove('error');
    
    // Remove ARIA role from the field
    field.removeAttribute('aria-describedby');

    // If the field is a radio button and part of a group, remove error from all and get the last item in the group
    if (field.type === 'radio' && field.name) {
        var group = field.form.querySelectorAll('[name="' + field.name + '"]');
        if (group.length > 0) {
            for (var i = 0; i < group.length; i++) {
                group[i].classList.remove('error');
            }
            field = group[group.length - 1];
        }
    }

    // Get field id or name
    var id = field.id || field.name;
    if (!id) {
        return;
    }
    
    // Check if an error message is in the DOM
    var message = field.form.querySelector('.mc-error#error-for-' + id + '');
    if (!message) {
        return;
    }

    // If so, hide it
    message.innerHTML = '';
    message.style.display = 'none';
    message.style.visibility = 'hidden';
};

// Listen to all blur events
document.addEventListener('blur', function (event) {

    if (event.target.form === undefined || event.target.form === null) {
        return;
    }

    // Only run if the field is in a form to be validated
    if (!event.target.classList.contains('validate')) {
        return;
    }

    // Validate the field
    var error = hasError(event.target);
  
    // If there's an error, show it
    if (error) {
        showError(event.target, error);
        return;
    }

    // Otherwise, remove any existing error message
    removeError(event.target);

}, true);

// Check all fields on submit
document.addEventListener('submit', function (event) {

    // Only run on forms flagged for validation
    var form = event.target;
    if (!form.classList.contains('validate')) {
        return;
    }

    // Prevent form from submitting
    event.preventDefault();

    // and validate the form
    validate(form);

}, false);

var validate = function (form) {
    // Get all of the form elements
    var fields = form.elements;

    // Validate each field
    // Store the first field with an error to a variable so we can bring it into focus later
    var error, hasErrors;
    for (var i = 0; i < fields.length; i++) {
        error = hasError(fields[i]);
        if (error) {
            showError(fields[i], error);
            if (!hasErrors) {
                hasErrors = fields[i];
            }
        }
    }
 
    // If there are errrors, don't submit form and focus on first element with error
    // Otherwise, let the form submit normally
    if (hasErrors) {
        hasErrors.focus();
    } else {
        removeAllErrors(form);
        submitMailChimpForm(form);
    }
};

//Submit

// Serialize the form data into a query string
// Forked and modified from https://stackoverflow.com/a/30153391/1293256
var serialize = function (form) {

    // Setup our serialized data
    var serialized = '';

    // Loop through each field in the form
    for (i = 0; i < form.elements.length; i++) {
        var field = form.elements[i];

        // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
        if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') {
            continue;
        }

        // Convert field data to a query string
        if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
            serialized += '&' + encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
        }
    }

    return serialized;

};

// Display the form status
var displayStatus = function (data) {

    // The form
    var mcForm = document.querySelector('.mc-form.submitting');
    if (mcForm === undefined || mcForm === null) {
        return;
    }

    var mcFormWrapper = document.querySelector('.mc-wrapper');

    // Get the status message content area
    var mcStatus = mcForm.querySelector('.mc-status');
    if (!mcStatus) {
        return;
    }

    var mcStatusMessage = mcStatus.querySelector('.mc-message');
    var vpMessage_success = mcStatus.querySelector('.vp-message-success');
    var vpMessage_error = mcStatus.querySelector('.vp-message-error');

    // Update our status message
    mcStatusMessage.innerHTML = data.msg;

    // Bring our status message into focus
    //mcStatus.addAttribute('tabindex', '-1');
    //mcStatus.focus();

    // If error, add error class
    if (data.result === 'error') {
        mcForm.classList.remove('submitting');
        mcForm.classList.add('error');
        mcFormWrapper.classList.remove('success');
        mcFormWrapper.classList.add('error');
        mcStatus.classList.remove('success');
        mcStatus.classList.add('error');
        vpMessage_success.style.display = 'none';
        vpMessage_error.style.display = 'none';
        mcStatusMessage.style.display = 'block';
        return;
    }

    // Otherwise, add success class
    mcForm.classList.remove('submitting');
    mcForm.classList.add('sended');
    mcFormWrapper.classList.remove('error');
    mcFormWrapper.classList.add('success');
    mcStatus.classList.remove('error');
    mcStatus.classList.add('success');
    mcStatusMessage.style.display = 'none';
    vpMessage_success.style.display = 'block';
    vpMessage_error.style.display = 'none';

    disableMailChimpForm(mcForm);
};

// Submit the form
var submitMailChimpForm = function (form) {
    
    if (!form.classList.contains('submitting')) {
        form.classList.add('submitting');
    }

    // Get the Submit URL
    var url = form.getAttribute('action');
    url = url.replace('/post?u=', '/post-json?u=');
    url += serialize(form) + '&c=displayStatus';
    addScript(url);
};

// Disable the form
var disableMailChimpForm = function (form) {
    var elements = form.elements;
    for (var i = 0, len = elements.length; i < len; ++i) {
        elements[i].disabled = true;
    }
}

// Load a script
var addScript = function (url) {
    // Create script with url and callback (if specified)
    var script = window.document.createElement('script');
    script.src = url;

    // Insert script tag into the DOM (append to <head>)
    var ref = window.document.getElementsByTagName('script')[ 0 ];
    ref.parentNode.insertBefore(script, ref);
}
