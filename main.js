(function($) {
    var videos = {
        '1': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-1L.mp4',
        '2A': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-2AL01.mp4',
        '2B': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-2BL.mp4',
        '3A': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-3AL.mp4',
        '3B': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-3BL.mp4',
        '4A': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-4AL.mp4',
        '4B': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-4BL.mp4',
        '5A': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-5AL.mp4',
        '5B': 'https://www.l214.com/wp-content/uploads/2019/12/Au-bout-du-conte-scene-5BL.mp4'
    };

    var srcs = {
        // Level 2
        11: videos['2A'],
        12: videos['2B'],
        // Level 3
        111: videos['3A'],
        112: videos['3B'],
        121: '',
        122: '',
        //Level 4
        1111: videos['4A'],
        1112: videos['4B'],
        1121: videos['4A'],
        1122: videos['4B'],
        // Level 5
        11111: videos['5A'],
        11112: videos['5B'],
        11121: videos['5A'],
        11122: videos['5B'],
        11211: videos['5A'],
        11212: videos['5B'],
        11221: videos['5A'],
        11222: videos['5B'],
    };
    var actions = {
        '11': 'Un mâle',
        '12': 'Une femelle', 
        '2A1': 'Rester avec mes copains coûte que coûte',
        '2A2': 'Par pitié, partir loin de cet enfer&nbsp;!',
        '2B1': 'Poursuivre en tant que mâle',
        '31': 'Si, mais avec toute cette nourriture à volonté, j’ai du mal à me contrôler&nbsp;!',
        '32': 'Non, cette vie me plaît bien&nbsp;!',
        '41': 'Oui, partir enfin&nbsp;! Rien ne peut être pire que ça.',
        '42': 'Oui, on retourne à l’élevage&nbsp;!',
        '51': 'Oui',
        '52': 'Non'
    }
    var btns_label = {
        // Level 2
        11: actions['11'],
        12: actions['12'],
        // Level 3
        111: actions['2A1'],
        112: actions['2A2'],
        121: actions['2B1'],
        122: '',
        // Level 4
        1111: actions['31'],
        1112: actions['32'],
        1121: actions['31'],
        1122: actions['32'],
        // Level 5
        11111: actions['41'],
        11112: actions['42'],
        11211: actions['41'],
        11212: actions['42'],
        11121: actions['41'],
        11122: actions['42'],
        11221: actions['41'],
        11222: actions['42'],
        // Level 6
        111111: actions['51'],
        111112: actions['52'],
        111121: actions['51'],
        111122: actions['52'],
        112111: actions['51'],
        112112: actions['52'],
        112121: actions['51'],
        112122: actions['52'],
        111211: actions['51'],
        111212: actions['52'],
        111221: actions['51'],
        111222: actions['52'],
        112211: actions['51'],
        112212: actions['52'],
        112221: actions['51'],
        112222: actions['52'],
    };

    var btns_redirection = {
        121: 11
    };

    var INITIAL_LEVEL = 1;
    var LAST_LEVEL = 6;
    var RELOAD_BEFORE = true;
    var DEBUG = true;
    var WITH_SENTRY = withSentry();

    var current_level = INITIAL_LEVEL;
    var initial_loaded = false;
    var spinner_timeout = null;

    function set_video(video, level) {
        if (level in srcs) {
            if (srcs[level] === '') {
                trace('Video soure level ' + level + ' is undefined.');
                return false;
            }
            video[0].src = srcs[level];
            return true;
        } else {
            trace('Video source level ' + level + ' not found.');
        }
        return false;
    }

    function load_video(pos, level) {
        var video_container = $('#video-' + pos);
        if (video_container.length <= 0) {
            trace('Video container #video-' + pos + ' not found.');
            return false;
        }

        var loaded = false;
        var video = video_container.find('video');
        if (level > INITIAL_LEVEL) {
            loaded = set_video(video, level);
        }
        if (level === INITIAL_LEVEL || loaded) {
            video[0].load();
            loaded = true;
        }
        return loaded;
    }

    function loading(show) {
        var spinner = $('#video-spinner');
        if (spinner.length > 0) {
            if (show) {
                spinner.show();
            }
            else {
                spinner.hide();
            }
        }
    }    

    // Video preloading
    function initialize_video_preloading(source) {
        if (source.length <= 0) {
            trace('Invalid video source ' +  source + ' could not be cloned.');
            return false;
        }
        // Build video container clones
        var clone1 = source.clone(true, true)
        .attr('id', 'video-1')
        .addClass('is-hidden')
        .insertAfter(source);
        clone1.find('.video-overlay').remove();
        
        var clone2 = source.clone(true, true)
        .attr('id', 'video-2')
        .addClass('is-hidden')
        .insertAfter(source);
        clone2.find('.video-overlay').remove();
    }

    function get_current_video() {
        var video_main = $('#video-main');
        if (video_main.length <= 0) {
            trace('Current video container #video-main not found.');
            return false;
        }
        return video_main.find('video');
    }

    function get_next_video(pos) {
        var video_loaded_container = $('#video-' + pos);
        if (video_loaded_container.length <= 0) {
            trace('Video container #video-' + pos + 'not found.');
            return false;
        }

        $('#video-main').addClass('is-hidden');
        video_loaded_container.removeClass('is-hidden');
        return video_loaded_container.find('video');
    }

    function load_first_videos() {
        if (initial_loaded) {
            return true;
        }
        initial_loaded = load_video(1, 11) && load_video(2, 12);
        return initial_loaded;
    }

    function load_next_videos(pos) {
        // Re-dispatch video containers
        var video_main_tmp = $('#video-main');
        if (video_main_tmp.length <= 0) {
            trace('Main video container #video-main not found.');
            return false;
        }
        var video_pos = $('#video-' + pos);
        if (video_pos.length <= 0) {
            trace('Video container #video-' + pos + 'not found.');
            return false;
        }
        
        video_main_tmp.attr('id', 'video-tmp');
        video_pos.attr('id', 'video-main');
        video_main_tmp.attr('id', 'video-' + pos);
        
        // Preload both next videos
        var loaded = false;
        var button_pos = $('.video-btn[data-pos=' + pos + ']');
        if (button_pos.length <= 0) {
            trace('Video action for ' + pos + 'not found.');
            return false;
        }
        var level = parseInt(button_pos.attr('data-level'));
        if (level >= INITIAL_LEVEL) {
            loaded = load_video(pos, level);
        }
        
        var sibling_pos = pos%2 + 1;
        var button_sibling = $('.video-btn[data-pos=' + sibling_pos + ']');
        if (button_sibling.length <= 0) {
            trace('Video action for ' + sibling_pos + ' not found.');
            return false;
        }
        level = parseInt(button_sibling.attr('data-level'));
        if (level >= INITIAL_LEVEL) {
            loaded = load_video(sibling_pos, level);
        }

        return loaded;
    }

    function prepare_next_buttons(level) {
        var has_next_buttons = false;
        $('.video-btn').each(function(index) {
            var next_level = level * 10 +  parseInt($(this).attr('data-pos'));
            if (next_level in btns_label && btns_label[next_level]) {
                // Level
                if (next_level in btns_redirection) {
                    $(this).attr('data-level', btns_redirection[next_level]);
                }
                else {
                    $(this).attr('data-level', next_level);
                }
                
                // Text
                $(this).html(btns_label[next_level]);
                
                $(this).show();
                has_next_buttons = true;
            }
            else {
                $(this).attr('data-level', 0);
                $(this).hide();
            }
        });
        
        return has_next_buttons;
    }

    function onLastStep(pos) {
        $('#video').fadeOut(800, 'swing', function() {
            $('#end-' + pos).fadeIn(800);
            $('#share').fadeIn(800);
            $('#content').fadeIn(800);
            $('#footer').fadeIn(800);
            // Hack
            //$(window).scroll();
            $(window).scrollTop($(window).scrollTop()+1);
        });
    }

    $(document).ready(function () {    
        // Main videos
        var video_main = $('#video-main');
        if (video_main.length <= 0) {
            trace('Main video container #video-main not found.');
            return false;
        }

        var video_actions = $('#video-actions');
        // Add a loading spinner
        if ($('#video-spinner').length === 0) {
            var spinner = $('<span id="video-spinner" class="video-spinner"></span>').hide();
            video_main.append(spinner);
        }
        
        // Init & preload
        var video = video_main.find('video');
        if (video.length <= 0) {
            return;
        }
        video.attr('type', 'video/mp4')
        video.attr('preload', 'auto');
        video.attr('playsinline', '');

        video.on('waiting',function() {
            spinner_timeout = setTimeout(function() {loading(true);}, 1000);
        });
        
        video.on('canplaythrough',function() {
            if (current_level === INITIAL_LEVEL) {
                if (load_first_videos()) {
                    $(this).off('canplaythrough');

                     // Display now the video container
                    var video_container = $(".video-viewer");
                    if (video_container.length > 0) {
                        video_container.removeClass('is-hidden');
                    }
                }
            }
        });

        video.on('canplay canplaythrough playing',function() {
            clearTimeout(spinner_timeout);
            loading(false);
        });
        
        video.on('ended',function() {
            //If canplaythrough has not been received on first video, do the the job now
            if (current_level === INITIAL_LEVEL) {
                load_first_videos();
            }
            video_actions.show();
        });

        // Handle video action buttons events.
        
        if (RELOAD_BEFORE) {
            initialize_video_preloading(video_main);
        }
        load_video('main', INITIAL_LEVEL);
        $('.video-btn').on('click', function() {
            video_actions.hide();
            
            var pos = parseInt($(this).attr('data-pos'));
            var level = parseInt($(this).attr('data-level'));
            if (current_level < LAST_LEVEL && level in srcs) {
                // Prepare next buttons
                prepare_next_buttons(level);
                
                // Update current and preloaded videos
                if (RELOAD_BEFORE) {
                    video = get_next_video(pos);
                    if (video && video.length > 0) {
                        load_next_videos(pos);
                    }
                } else {
                    video = get_current_video();
                    set_video(video, level);
                }
                
                // Play current video
                if (video && video.length > 0) {
                    video[0].play();
                }
                current_level++;
            } else {
                onLastStep(pos);
            }
        });

        prepare_next_buttons(INITIAL_LEVEL);

        // All videos
        $('.video-overlay').on('click', function() {
            var video_att = $(this).attr('data-video');
            var video = $(video_att);
            if (video.length > 0) {
                video[0].play();
                $(this).hide();
            }
        });
    });

    // Tools
    function withSentry() {
        if (typeof Sentry === "undefined") {
            return false;
        }
        Sentry.init({
            dsn: 'https://720b1e4f30544da6826f95d51bfbdd7c@o233961.ingest.sentry.io/5465941',

            // We recommend adjusting this value in production, or using tracesSampler
            // for finer control
            tracesSampleRate: 1.0,
        });
        return true;
    }

    function trace(message) {
        if (DEBUG) {
            if (WITH_SENTRY) {   
                Sentry.captureException(message);
            }
            console.log(message);
        }
    }
})(jQuery);


